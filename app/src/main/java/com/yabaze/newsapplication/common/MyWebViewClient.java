package com.yabaze.newsapplication.common;

import android.app.Activity;
import android.graphics.Bitmap;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yabaze.newsapplication.common.CommonUtilities;

public class MyWebViewClient extends WebViewClient {

    private CommonUtilities customAlertDialogs = new CommonUtilities();
    private WebViewError webViewError;
    Activity mContext;

    public MyWebViewClient(Activity mContext, WebViewError webViewError) {
        this.mContext = mContext;
        this.webViewError = webViewError;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);

        customAlertDialogs.showProgressDialog(mContext, "Loading Data..!");

    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (customAlertDialogs != null) {
            customAlertDialogs.cancelProgressDialog();
        }
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        if (customAlertDialogs != null) {
            customAlertDialogs.cancelProgressDialog();
        }
        webViewError.onWebLoadFailed();
    }

    public interface WebViewError {
        void onWebLoadFailed();
    }

}
