package com.yabaze.newsapplication.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.yabaze.newsapplication.R;

public class CommonUtilities {

    public Dialog dialog;

    public void showProgressDialog(Activity activity, String title) {

        try {

            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
            LayoutInflater inflater = activity.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.progress, null);
            TextView tv_title = dialogView.findViewById(R.id.loading_msg);

            tv_title.setText(title);
            builder.setView(dialogView);
            builder.setCancelable(false);

            dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelProgressDialog() {

        try {
            if (dialog != null) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = null;

        if (connectivityManager != null) {

            activeNetwork = connectivityManager.getActiveNetworkInfo();

        }

        return activeNetwork != null && activeNetwork.isConnected();

    }

    public static void showNoInternetConnectionDialog(final Context context) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View dialogView = null;
        if (inflater != null) {
            dialogView = inflater.inflate(R.layout.internet_connection_dialog, null);

            dialog.setView(dialogView);

            dialog.setCancelable(false);

            TextView btn_openSetting = dialogView.findViewById(R.id.btn_openSetting);
            TextView txt_dismiss = dialogView.findViewById(R.id.txt_dismiss);

            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
            btn_openSetting.setBackgroundResource(outValue.resourceId);
            txt_dismiss.setBackgroundResource(outValue.resourceId);

            btn_openSetting.setVisibility(View.VISIBLE);

            final AlertDialog alertDialog = dialog.create();

            alertDialog.show();
            txt_dismiss.setOnClickListener(v -> alertDialog.dismiss());
            btn_openSetting.setOnClickListener(v -> {

                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                context.startActivity(i);
                alertDialog.dismiss();

            });
        } else {
            Toast.makeText(context, "Try Again..!", Toast.LENGTH_SHORT).show();
        }
    }
}
