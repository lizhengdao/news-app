package com.yabaze.newsapplication.event;

public class NetworkEventBus {

    private Boolean isNetworkAvailable;

    public NetworkEventBus(boolean isNetworkAvailable) {
        this.isNetworkAvailable = isNetworkAvailable;
    }

    public Boolean getNetworkAvailable() {
        return isNetworkAvailable;
    }

    public void setNetworkAvailable(Boolean networkAvailable) {
        isNetworkAvailable = networkAvailable;
    }
}
