package com.yabaze.newsapplication.views.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.yabaze.newsapplication.R;
import com.yabaze.newsapplication.databinding.NewsRecyclerViewBinding;
import com.yabaze.newsapplication.repositories.remote.dto.Article;
import com.yabaze.newsapplication.viewModels.HomePageViewModel;

import java.util.List;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

    List<Article> articleArrayList;
    HomePageViewModel homePageViewModel;

    public NewsRecyclerViewAdapter(List<Article> articleArrayList, HomePageViewModel homePageViewModel) {
        this.articleArrayList = articleArrayList;
        this.homePageViewModel = homePageViewModel;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.news_recycler_view,
                parent,
                false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(articleArrayList.get(position), homePageViewModel);
    }

    @Override
    public int getItemCount() {
        return articleArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        NewsRecyclerViewBinding newsRecyclerViewBinding;

        public ViewHolder(@NonNull NewsRecyclerViewBinding newsRecyclerViewBinding) {
            super(newsRecyclerViewBinding.getRoot());
            this.newsRecyclerViewBinding = newsRecyclerViewBinding;
        }

        public void bind(Article article, HomePageViewModel homePageViewModel) {
            newsRecyclerViewBinding.setArticle(article);
            newsRecyclerViewBinding.setViewModel(homePageViewModel);
        }
    }
}
