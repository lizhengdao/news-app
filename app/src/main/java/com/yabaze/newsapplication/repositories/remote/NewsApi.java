package com.yabaze.newsapplication.repositories.remote;

import com.yabaze.newsapplication.repositories.remote.dto.ApiResponsePojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface NewsApi {

    @GET("/v2/top-headlines")
    Call<ApiResponsePojo> headlineBasedOnCountry(@Query("country") String country, @Query("apiKey") String api);

    @GET
    Call<ApiResponsePojo> headlineBasedOnFilter(@Url String url);

}


